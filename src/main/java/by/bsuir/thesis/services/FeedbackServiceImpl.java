package by.bsuir.thesis.services;

import by.bsuir.thesis.dto.FeedbackDto;
import by.bsuir.thesis.models.Feedback;
import by.bsuir.thesis.models.User;
import by.bsuir.thesis.repositories.FeedbackRepository;
import by.bsuir.thesis.repositories.TestAttemptRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class FeedbackServiceImpl implements FeedbackService {
    private final FeedbackRepository repository;
    private final TestAttemptRepository testAttemptRepository;

    public FeedbackServiceImpl(FeedbackRepository repository, TestAttemptRepository testAttemptRepository) {
        this.repository = repository;
        this.testAttemptRepository = testAttemptRepository;
    }

    @Override
    public List<Feedback> getAll() {
        return null;
    }

    @Override
    public Optional<Feedback> getById(UUID id) {
        return repository.findById(id);
    }

    @Override
    public boolean create(FeedbackDto feedbackDto, User currentUser) {
        var testAttempt = testAttemptRepository.getByUserIdAndTestId(feedbackDto.getUserId(), feedbackDto.getTestId());
        if (testAttempt.isEmpty()) {
            return false;
        }

        var feedback = new Feedback();
        feedback.setAuthor(currentUser);
        feedback.setTestAttempt(testAttempt.get());
        feedback.setText(feedbackDto.getText());
        repository.save(feedback);

        return false;
    }

    @Override
    public boolean update(Feedback feedback, FeedbackDto feedbackDto) {
        return false;
    }

    @Override
    public boolean delete(Feedback feedback) {
        repository.delete(feedback);
        return true;
    }

    @Override
    public List<Feedback> getAllByTestAndUser(UUID testId, UUID userId) {
        var testAttempt = testAttemptRepository.getByUserIdAndTestId(userId, testId);
        if (testAttempt.isEmpty()) {
            return List.of();
        }

        return repository.findAllByTestAttemptOrderByCreatedAtAsc(testAttempt.get());
    }

    @Override
    public List<Feedback> getAllByUser(User user) {
        return repository.findAllByUser(user);
    }
}
