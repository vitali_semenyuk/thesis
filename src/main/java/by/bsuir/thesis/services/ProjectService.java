package by.bsuir.thesis.services;

import by.bsuir.thesis.dto.CreateProjectDto;
import by.bsuir.thesis.dto.UpdateProjectDto;
import by.bsuir.thesis.models.Project;
import by.bsuir.thesis.models.Test;
import by.bsuir.thesis.models.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectService extends CrudService<Project, Project, CreateProjectDto, UpdateProjectDto> {
    List<Project> getAllByUser(User user);
    Optional<Project> getByIdWithTests(UUID id);
    Optional<Project> getByIdWithUsers(UUID id);
    boolean assignTest(Project project, Test test);
    boolean unassignTest(Project project, Test test);
    boolean assignUser(Project project, User user);
    boolean unassignUser(Project project, User user);
}
