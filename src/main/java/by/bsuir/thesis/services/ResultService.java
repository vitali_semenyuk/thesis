package by.bsuir.thesis.services;

import by.bsuir.thesis.models.TestAttempt;

import java.util.List;

public interface ResultService {
    List<TestAttempt> getAll();
}
