package by.bsuir.thesis.services;

import by.bsuir.thesis.dto.ProjectUserDto;
import by.bsuir.thesis.dto.UpdateUserDto;
import by.bsuir.thesis.dto.CreateUserDto;
import by.bsuir.thesis.models.Project;
import by.bsuir.thesis.models.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserService extends UserDetailsService {
    List<User> getAll();
    Optional<User> getById(UUID id);
    boolean create(CreateUserDto userDto);
    boolean update(User user, UpdateUserDto userDto);
    boolean delete(User user);
    List<User> getAllByProject(Project project);
    List<ProjectUserDto> getAllForProject(Project project);
}
