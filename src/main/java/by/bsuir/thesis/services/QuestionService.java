package by.bsuir.thesis.services;

import by.bsuir.thesis.dto.QuestionDto;
import by.bsuir.thesis.models.Question;
import by.bsuir.thesis.models.Test;

import java.util.List;

public interface QuestionService extends CrudService<Question, Question, QuestionDto, QuestionDto> {
    List<Question> getAllByTest(Test test);
    boolean create(QuestionDto questionDto, Test test);
}
