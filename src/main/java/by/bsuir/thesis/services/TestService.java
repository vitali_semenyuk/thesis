package by.bsuir.thesis.services;

import by.bsuir.thesis.dto.*;
import by.bsuir.thesis.models.Project;
import by.bsuir.thesis.models.Test;
import by.bsuir.thesis.models.User;

import java.util.List;

public interface TestService extends CrudService<Test, ListTestDto, CreateTestDto, UpdateTestDto> {
    List<Test> getAllByProject(Project project);
    List<Test> getAllByUser(User user);
    List<ProjectTestDto> getAllWithProjects();
    List<TestWithResultDto> getAllByProjectForUser(Project project, User user);
}
