package by.bsuir.thesis.services;

import by.bsuir.thesis.models.*;
import by.bsuir.thesis.repositories.TestAttemptRepository;
import by.bsuir.thesis.repositories.UserAnswerRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class TestSolvingServiceImpl implements TestSolvingService {
    private final TestAttemptRepository testAttemptRepository;
    private final UserAnswerRepository userAnswerRepository;
    private final QuestionService questionService;

    public TestSolvingServiceImpl(TestAttemptRepository testAttemptRepository, UserAnswerRepository userAnswerRepository, QuestionService questionService) {
        this.testAttemptRepository = testAttemptRepository;
        this.userAnswerRepository = userAnswerRepository;
        this.questionService = questionService;
    }

    @Override
    public boolean isSolved(User user, Test test) {
        var attempt = testAttemptRepository.getByUserAndTest(user, test);
        return attempt.isPresent();
    }

    @Override
    @Transactional
    public boolean submit(User user, Test test, TestAnswersDto answersDto) {
        var attempt = testAttemptRepository.getByUserAndTest(user, test);
        if (attempt.isPresent()) {
            return false;
        }

        var userAnswers = answersDto.getAnswers().entrySet().stream().map((entry) -> {
            var question = questionService.getById(UUID.fromString(entry.getKey()));
            if (question.isEmpty()) {
                return null;
            }

            var userAnswer = new UserAnswer();
            userAnswer.setUser(user);
            userAnswer.setQuestion(question.get());
            userAnswer.setAnswer(entry.getValue());
            userAnswer.setCorrect(question.get().getCorrectAnswer().equals(userAnswer.getAnswer()));
            userAnswerRepository.save(userAnswer);
            return userAnswer;
        }).collect(Collectors.toList());

        var score = userAnswers.stream().filter(UserAnswer::getCorrect).count() * 100 / userAnswers.size();

        var newAttempt = new TestAttempt();
        newAttempt.setTest(test);
        newAttempt.setUser(user);
        newAttempt.setScore((int) score);
        testAttemptRepository.save(newAttempt);

        userAnswerRepository.saveAll(userAnswers);

        return true;
    }

    @Override
    public Map<UUID, UserAnswer> getAnswers(User user, Test test) {
        return userAnswerRepository.findAllByUserForTest(user, test).stream().collect(Collectors.toMap(ua -> ua.getQuestion().getId(), Function.identity()));
    }
}
