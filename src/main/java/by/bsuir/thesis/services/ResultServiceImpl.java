package by.bsuir.thesis.services;

import by.bsuir.thesis.models.TestAttempt;
import by.bsuir.thesis.repositories.TestAttemptRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ResultServiceImpl implements ResultService {
    private final TestAttemptRepository testAttemptRepository;

    public ResultServiceImpl(TestAttemptRepository testAttemptRepository) {
        this.testAttemptRepository = testAttemptRepository;
    }

    @Override
    public List<TestAttempt> getAll() {
        return testAttemptRepository.findAll();
    }
}
