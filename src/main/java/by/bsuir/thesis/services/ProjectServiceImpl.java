package by.bsuir.thesis.services;

import by.bsuir.thesis.dto.CreateProjectDto;
import by.bsuir.thesis.dto.UpdateProjectDto;
import by.bsuir.thesis.models.Project;
import by.bsuir.thesis.models.ProjectUser;
import by.bsuir.thesis.models.Test;
import by.bsuir.thesis.models.User;
import by.bsuir.thesis.repositories.ProjectRepository;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class ProjectServiceImpl implements ProjectService {
    private final ProjectRepository repository;

    public ProjectServiceImpl(ProjectRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Project> getAll() {
        return repository.findAllByDeletedAtIsNullOrderByCreatedAtAsc();
    }

    @Override
    public Optional<Project> getById(UUID id) {
        return repository.findByDeletedAtIsNullAndId(id);
    }

    @Override
    public boolean create(CreateProjectDto projectDto, User currentUser) {
        var project = new Project();
        project.setAuthor(currentUser);
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());

        repository.save(project);

        return true;
    }

    @Override
    public boolean update(Project project, UpdateProjectDto projectDto) {
        project.setName(projectDto.getName());
        project.setDescription(projectDto.getDescription());

        repository.save(project);

        return true;
    }

    @Override
    @Transactional
    public boolean delete(Project project) {
        repository.softDelete(project.getId());
        return true;
    }

    @Override
    public List<Project> getAllByUser(User user) {
        return repository.findAllByUserId(user.getId());
    }

    @Override
    public Optional<Project> getByIdWithTests(UUID id) {
        return repository.findByIdWithTests(id);
    }

    @Override
    public Optional<Project> getByIdWithUsers(UUID id) {
        return repository.findByIdWithUsers(id);
    }

    @Override
    public boolean assignTest(Project project, Test test) {
        project.getTests().add(test);

        try {
            repository.save(project);
        } catch (DataIntegrityViolationException error) {
            return false;
        }

        return true;
    }

    @Override
    public boolean unassignTest(Project project, Test test) {
        project.getTests().remove(test);

        repository.save(project);

        return true;
    }

    @Override
    public boolean assignUser(Project project, User user) {
        var projectUser = new ProjectUser();
        projectUser.setProject(project);
        projectUser.setUser(user);
        project.getUsers().add(projectUser);

        try {
            repository.save(project);
        } catch (EntityExistsException error) {
            return false;
        }

        return true;
    }

    @Override
    public boolean unassignUser(Project project, User user) {
        project.getUsers().removeIf(projectUser -> projectUser.getId().getUserId().equals(user.getId()));

        repository.save(project);

        return true;
    }
}
