package by.bsuir.thesis.services;

import by.bsuir.thesis.dto.FeedbackDto;
import by.bsuir.thesis.models.Feedback;
import by.bsuir.thesis.models.User;

import java.util.List;
import java.util.UUID;

public interface FeedbackService extends CrudService<Feedback, Feedback, FeedbackDto, FeedbackDto> {
    List<Feedback> getAllByTestAndUser(UUID testId, UUID userId);
    List<Feedback> getAllByUser(User user);
}
