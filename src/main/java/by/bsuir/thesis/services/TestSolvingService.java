package by.bsuir.thesis.services;

import by.bsuir.thesis.models.Test;
import by.bsuir.thesis.models.TestAnswersDto;
import by.bsuir.thesis.models.User;
import by.bsuir.thesis.models.UserAnswer;

import java.util.Map;
import java.util.UUID;

public interface TestSolvingService {
    boolean isSolved(User user, Test test);
    boolean submit(User user, Test test, TestAnswersDto answersDto);
    Map<UUID, UserAnswer> getAnswers(User user, Test test);
}
