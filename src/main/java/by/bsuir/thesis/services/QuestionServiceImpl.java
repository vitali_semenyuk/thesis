package by.bsuir.thesis.services;

import by.bsuir.thesis.dto.QuestionDto;
import by.bsuir.thesis.models.*;
import by.bsuir.thesis.repositories.QuestionRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class QuestionServiceImpl implements QuestionService {
    private final QuestionRepository repository;

    public QuestionServiceImpl(QuestionRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<Question> getAllByTest(Test test) {
        return repository.findAllByTestOrderByCreatedAtAsc(test);
    }

    @Override
    public List<Question> getAll() {
        return repository.findAll();
    }

    @Override
    public Optional<Question> getById(UUID id) {
        return repository.findById(id);
    }

    @Override
    public boolean create(QuestionDto questionDto, User currentUser) {
        return false;
    }

    @Override
    public boolean create(QuestionDto questionDto, Test test) {
        var question = new Question();
        question.setId(questionDto.getId());
        question.setTest(test);

        return update(question, questionDto);
    }

    @Override
    public boolean update(Question question, QuestionDto questionDto) {
        question.setType(questionDto.getType());
        question.setText(questionDto.getText());
        question.setCorrectAnswer(questionDto.getCorrectAnswer());

        if (question.getType() == QuestionType.OPTIONS) {
            question.setAnswers(questionDto.getAnswers().stream().map(answerDto -> {
                var answer = new Answer();
                answer.setQuestion(question);
                answer.setId(answerDto.getId());
                answer.setText(answerDto.getText());
                return answer;
            }).collect(Collectors.toList()));
        }

        repository.save(question);

        return true;
    }

    @Override
    @Transactional
    public boolean delete(Question question) {
        repository.delete(question);
        return true;
    }
}
