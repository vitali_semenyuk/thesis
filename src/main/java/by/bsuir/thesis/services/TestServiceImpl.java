package by.bsuir.thesis.services;

import by.bsuir.thesis.dto.*;
import by.bsuir.thesis.models.Project;
import by.bsuir.thesis.models.Test;
import by.bsuir.thesis.models.User;
import by.bsuir.thesis.repositories.TestRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class TestServiceImpl implements TestService {
    private final TestRepository repository;

    public TestServiceImpl(TestRepository repository) {
        this.repository = repository;
    }

    @Override
    public List<ListTestDto> getAll() {
        return repository.findAllWithQuestionsCount();
    }

    @Override
    public Optional<Test> getById(UUID id) {
        return repository.findByDeletedAtIsNullAndId(id);
    }

    @Override
    public boolean create(CreateTestDto createTestDto, User currentUser) {
        var test = new Test();
        test.setAuthor(currentUser);
        test.setName(createTestDto.getName());
        test.setDescription(createTestDto.getDescription());

        repository.save(test);

        return true;
    }

    @Override
    public boolean update(Test test, UpdateTestDto updateTestDto) {
        test.setName(updateTestDto.getName());
        test.setDescription(updateTestDto.getDescription());

        repository.save(test);

        return true;
    }

    @Override
    @Transactional
    public boolean delete(Test test) {
        repository.softDelete(test.getId());
        return true;
    }

    @Override
    public List<Test> getAllByProject(Project project) {
        return repository.findAllByProjectsAndDeletedAtIsNullOrderByCreatedAtAsc(project);
    }

    @Override
    public List<Test> getAllByUser(User user) {
        return repository.findAllByUser(user);
    }

    @Override
    public List<ProjectTestDto> getAllWithProjects() {
        return repository.findAllWithProject();
    }

    @Override
    public List<TestWithResultDto> getAllByProjectForUser(Project project, User user) {
        return repository.findAllByProjectWithResults(project, user);
    }
}
