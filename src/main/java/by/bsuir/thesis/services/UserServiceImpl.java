package by.bsuir.thesis.services;

import by.bsuir.thesis.dto.ProjectUserDto;
import by.bsuir.thesis.dto.UpdateUserDto;
import by.bsuir.thesis.dto.CreateUserDto;
import by.bsuir.thesis.models.Project;
import by.bsuir.thesis.models.Role;
import by.bsuir.thesis.models.User;
import by.bsuir.thesis.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    private final UserRepository repository;
    private PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Autowired
    @Lazy
    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repository
                .findByDeletedAtIsNullAndUsernameIgnoreCase(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found"));
    }

    @Override
    public List<User> getAll() {
        return repository.findAllByDeletedAtIsNullOrderByCreatedAtAsc();
    }

    @Override
    public Optional<User> getById(UUID id) {
        return repository.findByDeletedAtIsNullAndId(id);
    }

    @Override
    public boolean create(CreateUserDto userDto) {
        var user = new User();
        user.setFirstName(userDto.getFirstName().strip());
        user.setLastName(userDto.getLastName().strip());
        user.setUsername(userDto.getUsername().strip().toLowerCase(Locale.ROOT));
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));

        try {
            repository.save(user);
        } catch (DataIntegrityViolationException error) {
            return false;
        }

        return true;
    }

    @Override
    public boolean update(User user, UpdateUserDto userDto) {
        user.setFirstName(userDto.getFirstName());
        user.setLastName(userDto.getLastName());
        user.setUsername(userDto.getUsername());
        user.setPhone(userDto.getPhone());
        user.setLocation(userDto.getLocation());
        user.setSpeciality(userDto.getSpeciality());
        user.setAddress(userDto.getAddress());

        if (userDto.getAvatarId() != null) {
            user.setAvatarId(userDto.getAvatarId());
        }

        if (userDto.getRole() != null) {
            user.setRole(Role.valueOf(userDto.getRole().toUpperCase(Locale.ROOT)));
        }

        if (userDto.getPassword() != null) {
            user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        }

        try {
            repository.save(user);
        } catch (DataIntegrityViolationException error) {
            return false;
        }

        return true;
    }

    @Override
    @Transactional
    public boolean delete(User user) {
        repository.softDelete(user.getId());
        return true;
    }

    @Override
    public List<User> getAllByProject(Project project) {
        return repository.findAllByProjectId(project.getId());
    }

    @Override
    public List<ProjectUserDto> getAllForProject(Project project) {
        return repository.findAllForProject(project);
    }
}
