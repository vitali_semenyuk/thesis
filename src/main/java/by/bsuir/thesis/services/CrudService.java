package by.bsuir.thesis.services;

import by.bsuir.thesis.models.User;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CrudService<Model, ListDto, CreateDto, UpdateDto> {
    List<ListDto> getAll();
    Optional<Model> getById(UUID id);
    boolean create(CreateDto createDto, User currentUser);
    boolean update(Model model, UpdateDto updateDto);
    boolean delete(Model model);
}
