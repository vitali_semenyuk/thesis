package by.bsuir.thesis.repositories;

import by.bsuir.thesis.models.Feedback;
import by.bsuir.thesis.models.TestAttempt;
import by.bsuir.thesis.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface FeedbackRepository extends JpaRepository<Feedback, UUID> {
    List<Feedback> findAllByTestAttemptOrderByCreatedAtAsc(TestAttempt testAttempt);

    @Query("SELECT f " +
            "FROM Feedback f " +
            "JOIN f.testAttempt.user u " +
            "WHERE u = ?1 " +
            "ORDER BY f.createdAt ASC")
    List<Feedback> findAllByUser(User user);
}