package by.bsuir.thesis.repositories;

import by.bsuir.thesis.models.Question;
import by.bsuir.thesis.models.Test;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface QuestionRepository extends JpaRepository<Question, UUID> {
    List<Question> findAllByTestOrderByCreatedAtAsc(Test test);
}