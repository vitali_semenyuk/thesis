package by.bsuir.thesis.repositories;

import by.bsuir.thesis.dto.ProjectUserDto;
import by.bsuir.thesis.models.Project;
import by.bsuir.thesis.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    List<User> findAllByDeletedAtIsNullOrderByCreatedAtAsc();

    @Query("SELECT u " +
            "FROM User u " +
            "JOIN u.projects p " +
            "WHERE u.deletedAt IS NULL AND p.id.projectId = ?1 " +
            "ORDER BY u.createdAt ASC")
    List<User> findAllByProjectId(UUID projectId);

    @Query("SELECT u AS user, p.id.projectId AS projectId " +
            "FROM User u " +
            "LEFT JOIN u.projects p ON p.project = ?1 " +
            "WHERE u.deletedAt IS NULL AND u.role = 'USER'" +
            "ORDER BY u.createdAt ASC")
    List<ProjectUserDto> findAllForProject(Project project);

    Optional<User> findByDeletedAtIsNullAndId(UUID id);

    Optional<User> findByDeletedAtIsNullAndUsernameIgnoreCase(String username);

    @Query("UPDATE User u SET u.deletedAt = current_timestamp WHERE u.id = ?1")
    @Modifying
    void softDelete(UUID id);
}