package by.bsuir.thesis.repositories;

import by.bsuir.thesis.models.Test;
import by.bsuir.thesis.models.TestAttempt;
import by.bsuir.thesis.models.TestAttemptKey;
import by.bsuir.thesis.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TestAttemptRepository extends JpaRepository<TestAttempt, TestAttemptKey> {
    Optional<TestAttempt> getByUserAndTest(User user, Test test);
    Optional<TestAttempt> getByUserIdAndTestId(UUID userId, UUID testId);
}