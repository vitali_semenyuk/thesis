package by.bsuir.thesis.repositories;

import by.bsuir.thesis.dto.ListTestDto;
import by.bsuir.thesis.dto.ProjectTestDto;
import by.bsuir.thesis.dto.TestWithResultDto;
import by.bsuir.thesis.models.Project;
import by.bsuir.thesis.models.Test;
import by.bsuir.thesis.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface TestRepository extends JpaRepository<Test, UUID> {
    @Query("SELECT t.id AS id, t.name AS name, COUNT(q.id) AS questionsCount " +
            "FROM Test t " +
            "LEFT JOIN t.questions q " +
            "WHERE t.deletedAt IS NULL " +
            "GROUP BY t.id " +
            "ORDER BY t.createdAt ASC")
    List<ListTestDto> findAllWithQuestionsCount();

    @Query("SELECT t.id AS id, t.name AS name, t.description AS description, a.id.testId AS attemptId, a.score AS result " +
            "FROM Test t " +
            "LEFT JOIN t.testAttempts a ON a.user = ?2 " +
            "JOIN t.projects p " +
            "WHERE t.deletedAt IS NULL AND p = ?1 " +
            "ORDER BY t.createdAt ASC")
    List<TestWithResultDto> findAllByProjectWithResults(Project project, User user);

    List<Test> findAllByProjectsAndDeletedAtIsNullOrderByCreatedAtAsc(Project project);

    @Query("SELECT t " +
            "FROM Test t " +
            "JOIN t.testAttempts a ON a.user = ?1 " +
            "WHERE t.deletedAt IS NULL " +
            "ORDER BY t.createdAt ASC")
    List<Test> findAllByUser(User user);

    @Query("SELECT t.id AS id, t.name AS name, p.id AS projectId " +
            "FROM Test t " +
            "LEFT JOIN t.projects p " +
            "WHERE t.deletedAt IS NULL " +
            "ORDER BY t.createdAt ASC")
    List<ProjectTestDto> findAllWithProject();

    Optional<Test> findByDeletedAtIsNullAndId(UUID id);

    @Query("UPDATE Test t SET t.deletedAt = current_timestamp WHERE t.id = ?1")
    @Modifying
    void softDelete(UUID id);
}