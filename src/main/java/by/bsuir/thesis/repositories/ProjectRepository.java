package by.bsuir.thesis.repositories;

import by.bsuir.thesis.models.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface ProjectRepository extends JpaRepository<Project, UUID> {
    List<Project> findAllByDeletedAtIsNullOrderByCreatedAtAsc();

    @Query("SELECT p " +
            "FROM Project p " +
            "JOIN p.users u " +
            "WHERE p.deletedAt IS NULL AND u.id.userId = ?1 " +
            "ORDER BY p.createdAt ASC")
    List<Project> findAllByUserId(UUID userId);

    Optional<Project> findByDeletedAtIsNullAndId(UUID id);

    @Query("SELECT p FROM Project p LEFT JOIN FETCH p.tests WHERE p.id = ?1")
    Optional<Project> findByIdWithTests(UUID id);

    @Query("SELECT p FROM Project p LEFT JOIN FETCH p.users WHERE p.id = ?1")
    Optional<Project> findByIdWithUsers(UUID id);

    @Query("UPDATE Project p SET p.deletedAt = current_timestamp WHERE p.id = ?1")
    @Modifying
    void softDelete(UUID id);
}