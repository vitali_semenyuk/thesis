package by.bsuir.thesis.repositories;

import by.bsuir.thesis.models.Test;
import by.bsuir.thesis.models.User;
import by.bsuir.thesis.models.UserAnswer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.UUID;

public interface UserAnswerRepository extends JpaRepository<UserAnswer, UUID> {
    @Query("SELECT ua FROM UserAnswer ua JOIN ua.question q WHERE ua.user = ?1 AND q.test = ?2")
    List<UserAnswer> findAllByUserForTest(User user, Test test);
}