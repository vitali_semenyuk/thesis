package by.bsuir.thesis.dto;

import by.bsuir.thesis.models.QuestionType;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Data
public class QuestionDto implements Serializable {
    @NotNull
    private UUID id;

    @NotNull
    private QuestionType type;

    @NotBlank
    @Size(min = 8, max = 100)
    private String text;

    @NotBlank
    private String correctAnswer;

    private List<AnswerDto> answers;
}
