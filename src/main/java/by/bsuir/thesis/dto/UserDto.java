package by.bsuir.thesis.dto;

public interface UserDto {
    String getPassword();
    String getPasswordConfirmation();
}
