package by.bsuir.thesis.dto;

import java.util.UUID;

public interface TestWithResultDto {
    UUID getId();
    String getName();
    String getDescription();
    UUID getAttemptId();
    Integer getResult();
}
