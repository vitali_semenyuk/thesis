package by.bsuir.thesis.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

@Data
public class FeedbackDto {
    private UUID userId;
    private UUID testId;

    @NotBlank
    private String text;
}
