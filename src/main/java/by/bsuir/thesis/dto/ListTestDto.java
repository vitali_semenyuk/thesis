package by.bsuir.thesis.dto;

import java.util.UUID;

public interface ListTestDto {
    UUID getId();
    String getName();
    Integer getQuestionsCount();
}
