package by.bsuir.thesis.dto;

import by.bsuir.thesis.models.Test;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.UUID;

@Data
public class UpdateTestDto implements Serializable {
    private UUID id;

    @NotBlank
    @Size(min = 2, max = 32)
    private String name;

    @NotBlank
    @Size(min = 10, max = 300)
    private String description;

    public static UpdateTestDto fromTest(Test test) {
        var dto = new UpdateTestDto();
        dto.setId(test.getId());
        dto.setName(test.getName());
        dto.setDescription(test.getDescription());
        return dto;
    }
}
