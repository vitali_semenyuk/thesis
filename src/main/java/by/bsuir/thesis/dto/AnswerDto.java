package by.bsuir.thesis.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class AnswerDto {
    private UUID id;

    private String text;
}
