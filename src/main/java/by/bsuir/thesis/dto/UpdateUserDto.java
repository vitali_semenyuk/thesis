package by.bsuir.thesis.dto;

import by.bsuir.thesis.models.User;
import by.bsuir.thesis.validators.PasswordMatches;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Locale;
import java.util.UUID;

@Data
@PasswordMatches(message = "{errors.passwordsMismatch}")
public class UpdateUserDto implements Serializable, UserDto {
    private UUID id;

    @NotBlank
    @Size(min = 2, max = 32)
    private String firstName;

    @NotBlank
    @Size(min = 2, max = 32)
    private String lastName;

    @NotBlank
    @Size(min = 4, max = 32)
    private String username;

    @Size(min = 8, max = 32)
    private String password;

    private String passwordConfirmation;

    private String role;

    private String phone;

    private String location;

    private String speciality;

    private String address;

    private String avatarId;

    private String avatarUrl;

    private String fullName;

    public static UpdateUserDto fromUser(User user) {
        var dto = new UpdateUserDto();
        dto.setId(user.getId());
        dto.setFirstName(user.getFirstName());
        dto.setLastName(user.getLastName());
        dto.setUsername(user.getUsername());
        dto.setRole(user.getRole().name().toLowerCase(Locale.ROOT));
        dto.setPhone(user.getPhone());
        dto.setLocation(user.getLocation());
        dto.setSpeciality(user.getSpeciality());
        dto.setAddress(user.getAddress());
        dto.setAvatarId(user.getAvatarId());
        dto.setAvatarUrl(user.getAvatarUrl());
        dto.setFullName(user.getFullName());
        return dto;
    }
}
