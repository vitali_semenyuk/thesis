package by.bsuir.thesis.dto;

import by.bsuir.thesis.models.Project;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.UUID;

@Data
public class UpdateProjectDto implements Serializable {
    private UUID id;

    @NotBlank
    @Size(min = 2, max = 32)
    private String name;

    @NotBlank
    @Size(min = 10, max = 300)
    private String description;

    public static UpdateProjectDto fromProject(Project project) {
        var dto = new UpdateProjectDto();
        dto.setId(project.getId());
        dto.setName(project.getName());
        dto.setDescription(project.getDescription());
        return dto;
    }
}
