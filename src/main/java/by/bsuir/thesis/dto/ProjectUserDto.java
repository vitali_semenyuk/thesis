package by.bsuir.thesis.dto;

import by.bsuir.thesis.models.User;

import java.util.UUID;

public interface ProjectUserDto {
    User getUser();
    UUID getProjectId();
}
