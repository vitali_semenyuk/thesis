package by.bsuir.thesis.dto;

import java.util.UUID;

public interface ProjectTestDto {
    UUID getId();
    String getName();
    UUID getProjectId();
}
