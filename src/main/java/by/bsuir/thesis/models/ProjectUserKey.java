package by.bsuir.thesis.models;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.UUID;

@Data
@Embeddable
public class ProjectUserKey implements Serializable {
    @Column(nullable = false)
    private UUID projectId;

    @Column(nullable = false)
    private UUID userId;
}
