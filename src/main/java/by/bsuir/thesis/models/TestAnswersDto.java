package by.bsuir.thesis.models;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

@Data
public class TestAnswersDto {
    Map<String, String> answers = new HashMap<>();
}
