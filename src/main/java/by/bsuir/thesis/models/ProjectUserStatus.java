package by.bsuir.thesis.models;

public enum ProjectUserStatus {
    PENDING, APPROVED, REJECTED
}
