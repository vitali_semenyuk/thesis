package by.bsuir.thesis.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/feedback")
public class FeedbacksController {
    @GetMapping
    String index() {
        return "feedback/index";
    }
}
