package by.bsuir.thesis.controllers.api;

import by.bsuir.thesis.models.Feedback;
import by.bsuir.thesis.services.FeedbackService;
import by.bsuir.thesis.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController("apiFeedbackController")
@RequestMapping("/api/feedbacks")
public class FeedbacksController {
    private final FeedbackService feedbackService;
    private final UserService userService;

    public FeedbacksController(FeedbackService feedbackService, UserService userService) {
        this.feedbackService = feedbackService;
        this.userService = userService;
    }

    @GetMapping
    ResponseEntity<List<Feedback>> index(@RequestParam UUID userId) {
        var user = userService.getById(userId);
        if (user.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        var feedbacks = feedbackService.getAllByUser(user.get());
        return ResponseEntity.ok(feedbacks);
    }
}
