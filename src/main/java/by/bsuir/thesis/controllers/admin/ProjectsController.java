package by.bsuir.thesis.controllers.admin;

import by.bsuir.thesis.dto.CreateProjectDto;
import by.bsuir.thesis.dto.UpdateProjectDto;
import by.bsuir.thesis.models.User;
import by.bsuir.thesis.services.ProjectService;
import by.bsuir.thesis.services.TestService;
import by.bsuir.thesis.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.UUID;

@Controller("adminProjectsController")
@RequestMapping("/admin/projects")
public class ProjectsController {
    private final ProjectService projectService;
    private final TestService testService;
    private final UserService userService;

    public ProjectsController(ProjectService projectService, TestService testService, UserService userService) {
        this.projectService = projectService;
        this.testService = testService;
        this.userService = userService;
    }

    @GetMapping
    String index(Model model) {
        model.addAttribute("projects", projectService.getAll());
        return "admin/projects/index";
    }

    @GetMapping("/{id}")
    String show(@PathVariable UUID id, Model model) {
        var project = projectService.getById(id);

        if (project.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        model.addAttribute("project", project.get());
        model.addAttribute("tests", testService.getAllByProject(project.get()));
        model.addAttribute("users", userService.getAllByProject(project.get()));
        return "admin/projects/show";
    }

    @GetMapping("/new")
    String newProject(@ModelAttribute("project") CreateProjectDto projectDto) {
        return "admin/projects/new";
    }

    @PostMapping
    String create(@Valid @ModelAttribute("project") CreateProjectDto projectDto, BindingResult result, RedirectAttributes attributes) {
        var currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (result.hasErrors()) {
            return "admin/projects/new";
        }

        projectService.create(projectDto, currentUser);

        attributes.addFlashAttribute("flashSuccess", "alerts.projectCreated");
        return "redirect:/admin/projects";
    }

    @GetMapping("/{id}/edit")
    String edit(@PathVariable UUID id, Model model) {
        var project = projectService.getById(id);

        if (project.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        model.addAttribute("project", UpdateProjectDto.fromProject(project.get()));
        return "admin/projects/edit";
    }

    @PostMapping("/{id}")
    String update(@PathVariable UUID id, @Valid @ModelAttribute("project") UpdateProjectDto projectDto, BindingResult result, RedirectAttributes attributes) {
        var project = projectService.getById(id);

        if (project.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if (result.hasErrors()) {
            return "admin/projects/edit";
        }

        projectService.update(project.get(), projectDto);

        attributes.addFlashAttribute("flashSuccess", "alerts.projectUpdated");
        return "redirect:/admin/projects";
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@PathVariable UUID id) {
        var project = projectService.getById(id);

        if (project.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        projectService.delete(project.get());
    }
}
