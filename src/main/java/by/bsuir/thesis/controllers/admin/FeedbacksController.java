package by.bsuir.thesis.controllers.admin;

import by.bsuir.thesis.dto.FeedbackDto;
import by.bsuir.thesis.models.Feedback;
import by.bsuir.thesis.models.User;
import by.bsuir.thesis.services.FeedbackService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController("adminFeedbackController")
@RequestMapping("/admin/feedbacks")
public class FeedbacksController {
    private final FeedbackService feedbackService;

    public FeedbacksController(FeedbackService feedbackService) {
        this.feedbackService = feedbackService;
    }

    @GetMapping
    ResponseEntity<List<Feedback>> index(@RequestParam UUID testId, @RequestParam UUID userId) {
        var feedbacks = feedbackService.getAllByTestAndUser(testId, userId);
        return ResponseEntity.ok(feedbacks);
    }

    @PostMapping
    ResponseEntity<Object> create(@Valid @RequestBody FeedbackDto feedbackDto) {
        var currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        feedbackService.create(feedbackDto, currentUser);

        return ResponseEntity.ok(null);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Object> delete(@PathVariable UUID id) {
        var feedback = feedbackService.getById(id);

        if (feedback.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        feedbackService.delete(feedback.get());

        return ResponseEntity.ok(null);
    }
}
