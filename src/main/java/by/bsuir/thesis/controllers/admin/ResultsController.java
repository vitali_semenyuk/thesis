package by.bsuir.thesis.controllers.admin;

import by.bsuir.thesis.services.ResultService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin/results")
public class ResultsController {
    private final ResultService resultService;

    public ResultsController(ResultService resultService) {
        this.resultService = resultService;
    }

    @GetMapping
    String index(Model model) {
        model.addAttribute("results", resultService.getAll());
        return "admin/results/index";
    }
}
