package by.bsuir.thesis.controllers.admin;

import by.bsuir.thesis.dto.ProjectUserDto;
import by.bsuir.thesis.models.User;
import by.bsuir.thesis.services.ProjectService;
import by.bsuir.thesis.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/admin/projects/{projectId}/users")
public class ProjectUsersController {
    private final ProjectService projectService;
    private final UserService userService;

    public ProjectUsersController(ProjectService projectService, UserService userService) {
        this.projectService = projectService;
        this.userService = userService;
    }

    @GetMapping
    ResponseEntity<List<ProjectUserDto>> index(@PathVariable UUID projectId) {
        var project = projectService.getById(projectId);

        if (project.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        var users = userService.getAllForProject(project.get());
        return ResponseEntity.ok(users);
    }

    @PutMapping("/{id}")
    ResponseEntity<User> update(@PathVariable UUID projectId, @PathVariable UUID id) {
        var project = projectService.getByIdWithUsers(projectId);

        if (project.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        var user = userService.getById(id);

        if (user.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        projectService.assignUser(project.get(), user.get());

        return ResponseEntity.ok(null);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<User> delete(@PathVariable UUID projectId, @PathVariable UUID id) {
        var project = projectService.getByIdWithUsers(projectId);

        if (project.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        var user = userService.getById(id);

        if (user.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        projectService.unassignUser(project.get(), user.get());

        return ResponseEntity.ok(null);
    }
}
