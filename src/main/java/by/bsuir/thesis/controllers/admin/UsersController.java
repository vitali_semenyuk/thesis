package by.bsuir.thesis.controllers.admin;

import by.bsuir.thesis.dto.UpdateUserDto;
import by.bsuir.thesis.models.User;
import by.bsuir.thesis.services.ProjectService;
import by.bsuir.thesis.services.TestService;
import by.bsuir.thesis.services.UserService;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.UUID;

@Controller
@RequestMapping("/admin/users")
public class UsersController {
    private final UserService userService;
    private final ProjectService projectService;
    private final TestService testService;

    public UsersController(UserService userService, ProjectService projectService, TestService testService) {
        this.userService = userService;
        this.projectService = projectService;
        this.testService = testService;
    }

    @InitBinder
    public void allowEmptyStringBinding(WebDataBinder binder) {
        // Tell spring to set empty values as null instead of empty string.
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping
    String index(Model model) {
        model.addAttribute("users", userService.getAll());
        return "admin/users/index";
    }

    @GetMapping("/{id}")
    String show(@PathVariable UUID id, Model model) {
        var user = userService.getById(id);

        if (user.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        model.addAttribute("user", user.get());
        model.addAttribute("projects", projectService.getAllByUser(user.get()));
        model.addAttribute("tests", testService.getAllByUser(user.get()));
        return "admin/users/show";
    }

    @GetMapping("/{id}/edit")
    String edit(@PathVariable UUID id, Model model) {
        var user = userService.getById(id);

        if (user.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        model.addAttribute("user", UpdateUserDto.fromUser(user.get()));
        return "admin/users/edit";
    }

    @PostMapping("/{id}")
    String update(@PathVariable UUID id, @Valid @ModelAttribute("user") UpdateUserDto userDto, BindingResult result) {
        var user = userService.getById(id);

        if (user.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if (result.hasErrors()) {
            return "admin/users/edit";
        }

        if (!userService.update(user.get(), userDto)) {
            result.rejectValue("username", "errors.existingUsername");
            return "admin/users/edit";
        }

        return "redirect:/admin/users";
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@PathVariable UUID id) {
        var user = userService.getById(id);
        var currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (user.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if (currentUser.getId().equals(id)) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

        userService.delete(user.get());
    }
}
