package by.bsuir.thesis.controllers.admin;

import by.bsuir.thesis.dto.CreateTestDto;
import by.bsuir.thesis.dto.UpdateTestDto;
import by.bsuir.thesis.models.User;
import by.bsuir.thesis.services.QuestionService;
import by.bsuir.thesis.services.TestService;
import by.bsuir.thesis.services.TestSolvingService;
import by.bsuir.thesis.services.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.UUID;

@Controller("adminTestsController")
@RequestMapping("/admin/tests")
public class TestsController {
    private final TestService testService;
    private final QuestionService questionService;
    private final UserService userService;
    private final TestSolvingService testSolvingService;

    public TestsController(TestService testService, QuestionService questionService, UserService userService, TestSolvingService testSolvingService) {
        this.testService = testService;
        this.questionService = questionService;
        this.userService = userService;
        this.testSolvingService = testSolvingService;
    }

    @GetMapping
    String index(Model model) {
        model.addAttribute("tests", testService.getAll());
        return "admin/tests/index";
    }

    @GetMapping("/{id}")
    String show(@PathVariable UUID id, Model model) {
        var test = testService.getById(id);

        if (test.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        model.addAttribute("test", test.get());
        return "admin/tests/show";
    }

    @GetMapping("/new")
    String newTest(@ModelAttribute("test") CreateTestDto testDto) {
        return "admin/tests/new";
    }

    @PostMapping
    String create(@Valid @ModelAttribute("test") CreateTestDto testDto, BindingResult result, RedirectAttributes attributes) {
        var currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (result.hasErrors()) {
            return "admin/tests/new";
        }

        testService.create(testDto, currentUser);

        attributes.addFlashAttribute("flashSuccess", "alerts.testCreated");
        return "redirect:/admin/tests";
    }

    @GetMapping("/{id}/edit")
    String edit(@PathVariable UUID id, Model model) {
        var test = testService.getById(id);

        if (test.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        model.addAttribute("test", UpdateTestDto.fromTest(test.get()));
        return "admin/tests/edit";
    }

    @PostMapping("/{id}")
    String update(@PathVariable UUID id, @Valid @ModelAttribute("test") UpdateTestDto testDto, BindingResult result, RedirectAttributes attributes) {
        var test = testService.getById(id);

        if (test.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if (result.hasErrors()) {
            return "admin/tests/edit";
        }

        testService.update(test.get(), testDto);

        attributes.addFlashAttribute("flashSuccess", "alerts.testUpdated");
        return "redirect:/admin/tests";
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    void delete(@PathVariable UUID id) {
        var test = testService.getById(id);

        if (test.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        testService.delete(test.get());
    }

    @GetMapping("/{id}/results/{userId}")
    String results(@PathVariable UUID id, @PathVariable UUID userId, Model model) {
        var test = testService.getById(id);
        var user = userService.getById(userId);

        if (test.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if (user.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        model.addAttribute("test", test.get());
        model.addAttribute("questions", questionService.getAllByTest(test.get()));
        model.addAttribute("results", testSolvingService.getAnswers(user.get(), test.get()));
        model.addAttribute("isAdmin", true);
        return "tests/solve";
    }
}
