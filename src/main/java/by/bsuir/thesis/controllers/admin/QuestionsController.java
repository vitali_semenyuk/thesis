package by.bsuir.thesis.controllers.admin;

import by.bsuir.thesis.dto.QuestionDto;
import by.bsuir.thesis.models.Question;
import by.bsuir.thesis.services.QuestionService;
import by.bsuir.thesis.services.TestService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/admin/tests/{testId}/questions")
public class QuestionsController {
    private final QuestionService questionService;
    private final TestService testService;

    public QuestionsController(QuestionService questionService, TestService testService) {
        this.questionService = questionService;
        this.testService = testService;
    }

    @GetMapping
    public ResponseEntity<List<Question>> index(@PathVariable UUID testId) {
        var test = testService.getById(testId);

        if (test.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        var questions = questionService.getAllByTest(test.get());
        return ResponseEntity.ok(questions);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Question> upsert(@PathVariable UUID testId, @PathVariable UUID id, @Valid @RequestBody QuestionDto questionDto) {
        var test = testService.getById(testId);

        if (test.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        var question = questionService.getById(id);

        if (question.isPresent()) {
            questionService.update(question.get(), questionDto);
        } else {
            questionService.create(questionDto, test.get());
        }

        return ResponseEntity.ok(null);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable UUID testId, @PathVariable UUID id) {
        var test = testService.getById(testId);

        if (test.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        var question = questionService.getById(id);

        if (question.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        questionService.delete(question.get());
        return ResponseEntity.noContent().build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(
            MethodArgumentNotValidException ex) {
        var errors = new HashMap<String, String>();
        ex.getBindingResult().getAllErrors().forEach((error) -> {
            var fieldName = ((FieldError) error).getField();
            var errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }
}
