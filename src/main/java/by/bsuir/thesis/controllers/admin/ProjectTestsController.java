package by.bsuir.thesis.controllers.admin;

import by.bsuir.thesis.dto.ProjectTestDto;
import by.bsuir.thesis.models.Test;
import by.bsuir.thesis.services.ProjectService;
import by.bsuir.thesis.services.TestService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/admin/projects/{projectId}/tests")
public class ProjectTestsController {
    private final ProjectService projectService;
    private final TestService testsService;

    public ProjectTestsController(ProjectService projectService, TestService testsService) {
        this.projectService = projectService;
        this.testsService = testsService;
    }

    @GetMapping
    ResponseEntity<List<ProjectTestDto>> index(@PathVariable UUID projectId) {
        var project = projectService.getById(projectId);

        if (project.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        var tests = testsService.getAllWithProjects();
        return ResponseEntity.ok(tests);
    }

    @PutMapping("/{id}")
    ResponseEntity<Test> update(@PathVariable UUID projectId, @PathVariable UUID id) {
        var project = projectService.getByIdWithTests(projectId);

        if (project.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        var test = testsService.getById(id);

        if (test.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        projectService.assignTest(project.get(), test.get());

        return ResponseEntity.ok(null);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<Test> delete(@PathVariable UUID projectId, @PathVariable UUID id) {
        var project = projectService.getByIdWithTests(projectId);

        if (project.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        var test = testsService.getById(id);

        if (test.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        projectService.unassignTest(project.get(), test.get());

        return ResponseEntity.ok(null);
    }
}
