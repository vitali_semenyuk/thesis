package by.bsuir.thesis.controllers;

import by.bsuir.thesis.dto.CreateUserDto;
import by.bsuir.thesis.services.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping(path = "/auth")
public class AuthController {
    private final UserService userService;

    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/signup")
    String signUp(@ModelAttribute("user") CreateUserDto user) {
        return "auth/sign-up";
    }

    @PostMapping("/signup")
    String create(@Valid @ModelAttribute("user") CreateUserDto user, BindingResult result, RedirectAttributes attributes) {
        if (result.hasErrors()) {
            return "auth/sign-up";
        }

        if (!userService.create(user)) {
            result.rejectValue("username", "errors.existingUsername");
            return "auth/sign-up";
        }

        attributes.addFlashAttribute("flashSuccess", "alerts.signUp");
        return "redirect:/";
    }
}
