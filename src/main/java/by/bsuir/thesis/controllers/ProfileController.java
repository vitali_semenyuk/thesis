package by.bsuir.thesis.controllers;

import by.bsuir.thesis.dto.UpdateUserDto;
import by.bsuir.thesis.models.User;
import by.bsuir.thesis.services.ProjectService;
import by.bsuir.thesis.services.TestService;
import by.bsuir.thesis.services.UserService;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@Controller
@RequestMapping("/profile")
public class ProfileController {
    private final UserService userService;
    private final ProjectService projectService;
    private final TestService testService;

    public ProfileController(UserService userService, ProjectService projectService, TestService testService) {
        this.userService = userService;
        this.projectService = projectService;
        this.testService = testService;
    }

    @InitBinder
    public void allowEmptyStringBinding(WebDataBinder binder) {
        // Tell spring to set empty values as null instead of empty string.
        binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
    }

    @GetMapping
    String show(Model model) {
        var currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        var user = userService.getById(currentUser.getId());

        if (user.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        model.addAttribute("user", user.get());
        model.addAttribute("projects", projectService.getAllByUser(user.get()));
        model.addAttribute("tests", testService.getAllByUser(user.get()));
        return "profile/show";
    }

    @GetMapping("/edit")
    String edit(Model model) {
        var currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        var user = userService.getById(currentUser.getId());

        if (user.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        model.addAttribute("user", UpdateUserDto.fromUser(user.get()));
        return "profile/edit";
    }

    @PostMapping
    String update(@Valid @ModelAttribute("user") UpdateUserDto userDto, BindingResult result) {
        var currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        var user = userService.getById(currentUser.getId());

        if (user.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if (result.hasErrors()) {
            return "profile/edit";
        }

        if (!userService.update(user.get(), userDto)) {
            result.rejectValue("username", "errors.existingUsername");
            return "profile/edit";
        }

        return "redirect:/profile";
    }
}
