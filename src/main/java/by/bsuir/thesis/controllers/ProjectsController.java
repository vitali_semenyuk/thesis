package by.bsuir.thesis.controllers;

import by.bsuir.thesis.models.User;
import by.bsuir.thesis.services.ProjectService;
import by.bsuir.thesis.services.TestService;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@Controller
@RequestMapping("/projects")
public class ProjectsController {
    private final ProjectService projectService;
    private final TestService testService;

    public ProjectsController(ProjectService projectService, TestService testService) {
        this.projectService = projectService;
        this.testService = testService;
    }

    @GetMapping
    String index(Model model) {
        var currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        model.addAttribute("projects", projectService.getAllByUser(currentUser));
        return "projects/index";
    }

    @GetMapping("/{id}")
    String show(@PathVariable UUID id, Model model) {
        var currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        var project = projectService.getById(id);

        if (project.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        model.addAttribute("project", project.get());
        model.addAttribute("tests", testService.getAllByProjectForUser(project.get(), currentUser));
        return "projects/show";
    }
}
