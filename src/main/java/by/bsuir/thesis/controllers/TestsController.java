package by.bsuir.thesis.controllers;

import by.bsuir.thesis.models.TestAnswersDto;
import by.bsuir.thesis.models.User;
import by.bsuir.thesis.services.QuestionService;
import by.bsuir.thesis.services.TestService;
import by.bsuir.thesis.services.TestSolvingService;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.UUID;

@Controller
@RequestMapping("/tests")
public class TestsController {
    private final TestService testService;
    private final QuestionService questionService;
    private final TestSolvingService testSolvingService;

    public TestsController(TestService testService, QuestionService questionService, TestSolvingService testSolvingService) {
        this.testService = testService;
        this.questionService = questionService;
        this.testSolvingService = testSolvingService;
    }

    @GetMapping("/{id}")
    String show(@PathVariable UUID id, Model model) {
        var test = testService.getById(id);

        if (test.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        model.addAttribute("test", test.get());
        return "tests/show";
    }

    @GetMapping("/{id}/solve")
    String solve(@PathVariable UUID id, Model model) {
        var currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        var test = testService.getById(id);

        if (test.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if (testSolvingService.isSolved(currentUser, test.get())) {
            return String.format("redirect:/tests/%s/results", id);
        }

        model.addAttribute("test", test.get());
        model.addAttribute("questions", questionService.getAllByTest(test.get()));
        return "tests/solve";
    }

    @PostMapping("/{id}/solve")
    String validate(@PathVariable UUID id, TestAnswersDto params) {
        var currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        var test = testService.getById(id);

        if (test.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        testSolvingService.submit(currentUser, test.get(), params);

        return String.format("redirect:/tests/%s/results", id);
    }

    @GetMapping("/{id}/results")
    String results(@PathVariable UUID id, Model model) {
        var currentUser = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        var test = testService.getById(id);

        if (test.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }

        if (!testSolvingService.isSolved(currentUser, test.get())) {
            return String.format("redirect:/tests/%s/solve", id);
        }

        model.addAttribute("test", test.get());
        model.addAttribute("questions", questionService.getAllByTest(test.get()));
        model.addAttribute("results", testSolvingService.getAnswers(currentUser, test.get()));
        return "tests/solve";
    }
}
