package by.bsuir.thesis.validators;

import by.bsuir.thesis.dto.UserDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, UserDto> {
    @Override
    public void initialize(PasswordMatches constraintAnnotation) {

    }

    @Override
    public boolean isValid(UserDto userDto, ConstraintValidatorContext context) {
        var password = userDto.getPassword();
        var passwordConfirmation = userDto.getPasswordConfirmation();
        var isMatches = password == null || passwordConfirmation == null || password.equals(passwordConfirmation);
        if (isMatches) return true;

        context.disableDefaultConstraintViolation();
        context
                .buildConstraintViolationWithTemplate(context.getDefaultConstraintMessageTemplate())
                .addPropertyNode("passwordConfirmation")
                .addConstraintViolation();

        return false;
    }
}
