i18next.use(ReactI18next.initReactI18next)
    .init({
    lng: 'ru',
    resources: {
        en: {
            translation: {
                actions: {
                    add: 'Add',
                    remove: 'Remove',
                    delete: 'Delete',
                    edit: 'Edit',
                    save: 'Save',
                },
                constructor: {
                    addQuestion: 'Add question',
                    question: 'Question',
                    type: 'Question type',
                    typePlaceholder: 'Select type',
                    text: 'Question text',
                    textPlaceholder: 'Enter question text',
                    answers: 'Answers',
                    correctAnswer: 'Correct answer',
                    correctAnswerPlaceholder: 'Select correct answer',
                    answerText: 'Answer text',
                    answerTextPlaceholder: 'Enter answer text',
                    validationError: 'Cannot be saved because of validation errors',
                    types: {
                        options: 'Question with multiple answers',
                        precise: 'Precise answer',
                    },
                    empty: 'None',
                },
                feedback: {
                    send: 'Leave feedback',
                    placeholder: 'Write your feedback',
                },
                search: {
                    placeholder: 'Search',
                },
                user: {
                    location: 'Location',
                    speciality: 'Speciality',
                },
            }
        },
        ru: {
            translation: {
                actions: {
                    add: 'Добавить',
                    remove: 'Убрать',
                    delete: 'Удалить',
                    edit: 'Изменить',
                    save: 'Сохранить',
                },
                constructor: {
                    addQuestion: 'Добавить вопрос',
                    question: 'Вопрос',
                    type: 'Тип вопроса',
                    typePlaceholder: 'Выберите тип',
                    text: 'Текст вопроса',
                    textPlaceholder: 'Введите текст вопроса',
                    answers: 'Ответы',
                    correctAnswer: 'Правильный ответ',
                    correctAnswerPlaceholder: 'Выберите правильный ответ',
                    answerText: 'Текст ответа',
                    answerTextPlaceholder: 'Введите текст ответа',
                    validationError: 'Не сохранено из-за ошибок валидации',
                    types: {
                        options: 'Закрытый вопрос',
                        precise: 'Открытый вопрос',
                    },
                    empty: 'Нет',
                },
                feedback: {
                    send: 'Оставить отзыв',
                    placeholder: 'Напишите свой отзыв о работе',
                },
                search: {
                    placeholder: 'Поиск',
                },
                user: {
                    location: 'Местоположение',
                    speciality: 'Специальность',
                },
            }
        }
    }
});
