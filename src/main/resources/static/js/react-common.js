const SearchBar = ({ filter, setFilter }) => {
    const { t } = ReactI18next.useTranslation();

    const handleChange = (event) => {
        setFilter(event.target.value);
    };

    return (
        <div className="uk-margin uk-text-center">
            <form className="uk-search uk-search-default">
                <span uk-search-icon="true"></span>
                <input
                    className="uk-search-input"
                    type="search"
                    placeholder={t('search.placeholder')}
                    value={filter}
                    onChange={handleChange}
                />
            </form>
        </div>
    );
};
