const testsApi = {
    fetch(projectId) {
        return fetch(`/admin/projects/${projectId}/tests`)
            .then((response) => response.json());
    },
    assign(projectId, testId) {
        return fetch(`/admin/projects/${projectId}/tests/${testId}`, {
            method: 'PUT',
            headers: getCSRFHeaders(),
        });
    },
    unassign(projectId, testId) {
        return fetch(`/admin/projects/${projectId}/tests/${testId}`, {
            method: 'DELETE',
            headers: getCSRFHeaders(),
        });
    },
};

const TestsSelectorAppContext = React.createContext({});

const Test = ({ test }) => {
    const { projectId, updateTest } = React.useContext(TestsSelectorAppContext);
    const { t } = ReactI18next.useTranslation();

    const handleAddClick = () => {
        testsApi.assign(projectId, test.id).then(() => {
            updateTest(test.id, { projectId });
        });
    };
    const handleRemoveClick = () => {
        testsApi.unassign(projectId, test.id).then(() => {
            updateTest(test.id, { projectId: null });
        });
    };

    return (
        <div className="uk-card uk-card-default uk-card-body uk-margin">
            <p>{test.name}</p>
            {test.projectId === projectId ? (
                <button className="uk-button uk-button-danger" onClick={handleRemoveClick}>{t('actions.remove')}</button>
            ) : (
                <button className="uk-button uk-button-primary" onClick={handleAddClick}>{t('actions.add')}</button>
            )}
        </div>
    );
};

const TestList = ({ tests }) => {
    const { filter } = React.useContext(TestsSelectorAppContext);
    const assigned = tests.filter(({ projectId }) => projectId);
    const unassigned = tests.filter(({ projectId }) => !projectId);
    const collection = [...assigned, ...unassigned].filter(({ name }) => name.toLowerCase().includes(filter.toLowerCase()));

    return (
        <div className="uk-height-max-large uk-overflow-auto uk-padding">
            {collection.map((test) => (
                <Test key={test.id} test={test} />
            ))}
        </div>
    );
};

const TestsSearchBar = () => {
    const { filter, setFilter } = React.useContext(TestsSelectorAppContext);

    return (
        <SearchBar filter={filter} setFilter={setFilter} />
    );
};

const TestsSelectorApp = ({ projectId }) => {
    const [tests, setTests] = React.useState([]);
    const [isLoading, setLoading] = React.useState(true);
    const [filter, setFilter] = React.useState('');

    const updateTest = (testId, diff) => {
        setTests(
            tests.map((test) => {
                if (test.id !== testId) return test;

                return { ...test, ...diff };
            })
        );
    };

    React.useEffect(() => {
        testsApi.fetch(projectId).then((response) => {
            setTests(response);

            setTimeout(() => {
                setLoading(false);
            }, 1000);
        });
    }, []);

    if (isLoading) {
        return (
            <div className="uk-text-center uk-padding">
                <div uk-spinner="ratio: 3" />
            </div>
        );
    }

    return (
        <TestsSelectorAppContext.Provider value={{ projectId, filter, setFilter, updateTest }}>
            <TestsSearchBar />
            <TestList tests={tests} />
        </TestsSelectorAppContext.Provider>
    );
};

const launchTestsSelector = () => {
    const root = document.getElementById('modal-body');
    const projectId = root.dataset.projectId;
    ReactDOM.render(<TestsSelectorApp projectId={projectId} />, root);
};
