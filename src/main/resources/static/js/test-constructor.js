const api = {
    fetchQuestions(testId) {
        return fetch(`/admin/tests/${testId}/questions`)
            .then((response) => response.json());
    },
    upsertQuestion(testId, question) {
        return fetch(`/admin/tests/${testId}/questions/${question.id}`, {
            method: 'PUT',
            headers: {
                ...getCSRFHeaders(),
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ ...question, type: question.type && question.type.toUpperCase() }),
        }).then((response) => {
            if (!response.ok) {
                const error = new Error('Request failed');
                error.response = response;
                throw error;
            }

            return response;
        });
    },
    removeQuestion(testId, questionId) {
        return fetch(`/admin/tests/${testId}/questions/${questionId}`, {
            method: 'DELETE',
            headers: getCSRFHeaders(),
        }).then((response) => {
            if (!response.ok) {
                throw new Error('Request failed');
            }

            return response;
        });
    },
};

const AppContext = React.createContext({});

const QuestionTypeSelector = ({ question }) => {
    const { questionsErrors, updateQuestion } = React.useContext(AppContext);
    const { t } = ReactI18next.useTranslation();
    const type = question.type || '';
    const error = questionsErrors[question.id] && questionsErrors[question.id].type;

    const handleChange = (e) => {
        updateQuestion(question.id, { type: e.target.value });
    };

    return (
        <div className="uk-margin">
            <label className="uk-form-label" htmlFor={`question-${question.id}-type`}>{t('constructor.type')}</label>
            <div className="uk-form-controls">
                <select
                    className={`uk-select ${error ? 'uk-form-danger' : ''}`}
                    id={`question-${question.id}-type`}
                    value={type}
                    onChange={handleChange}
                    required
                >
                    <option value="" disabled hidden>{t('constructor.typePlaceholder')}</option>
                    <option value="options">{t('constructor.types.options')}</option>
                    <option value="precise">{t('constructor.types.precise')}</option>
                </select>
                {error && <div className="uk-text-small uk-text-danger">{error}</div>}
            </div>
        </div>
    );
};

const QuestionText = ({ question }) => {
    const { questionsErrors, updateQuestion } = React.useContext(AppContext);
    const { t } = ReactI18next.useTranslation();
    const error = questionsErrors[question.id] && questionsErrors[question.id].text;

    const handleChange = (e) => {
        updateQuestion(question.id, { text: e.target.value });
    };

    return (
        <div className="uk-margin">
            <label className="uk-form-label" htmlFor={`question-${question.id}-text`}>{t('constructor.text')}</label>
            <div className="uk-form-controls">
                <input
                    type="text"
                    className={`uk-input ${error ? 'uk-form-danger' : ''}`}
                    id={`question-${question.id}-text`}
                    placeholder={t('constructor.textPlaceholder')}
                    value={question.text}
                    onChange={handleChange}
                    required
                />
                {error && <div className="uk-text-small uk-text-danger">{error}</div>}
            </div>
        </div>
    );
};

const Answer = ({ question }) => {
    if (question.type === 'options') return <AnswerWithOptions question={question} />
    if (question.type === 'precise') return <AnswerPrecise question={question} />

    return null;
};

const AnswerWithOptions = ({ question }) => {
    const canAdd = question.answers.length < 5;
    const canRemove = question.answers.length > 2;

    const { updateQuestion } = React.useContext(AppContext);
    const handleAddAnswer = () => {
        if (!canAdd) return;

        updateQuestion(question.id, { answers: [...question.answers, { id: uuidv4(), text: '' }] });
    };
    const handleChangeAnswer = (answerId, value) => {
        updateQuestion(question.id, {
            answers: question.answers.map((answer) => {
                if (answer.id !== answerId) return answer;

                return { ...answer, text: value };
            })
        });
    };
    const handleRemoveAnswer = () => {
        if (!canRemove) return;

        updateQuestion(question.id, { answers: question.answers.slice(0, -1) });
    };

    return (
        <React.Fragment>
            <div>
                {question.answers.map((answer, index) => (
                    <AnswerText key={answer.id} index={index + 1} answer={answer} onChange={handleChangeAnswer} />
                ))}
            </div>
            <div className="uk-flex uk-flex-center">
                <a
                    href="#"
                    className={`uk-icon-button uk-margin-right ${canAdd ? 'uk-button-primary' : ''}`}
                    uk-icon="plus"
                    onClick={handleAddAnswer}
                />
                <a
                    href="#"
                    className={`uk-icon-button uk-margin-left ${canRemove ? 'uk-button-danger' : ''}`}
                    uk-icon="minus"
                    onClick={handleRemoveAnswer}
                />
            </div>
            <CorrectAnswerSelector question={question} />
        </React.Fragment>
    );
};

const CorrectAnswerSelector = ({ question }) => {
    const correctAnswer = question.correctAnswer || 'undefined';
    const answers = question.answers.filter(({ text }) => !!text);
    const { questionsErrors, updateQuestion } = React.useContext(AppContext);
    const { t } = ReactI18next.useTranslation();
    const error = questionsErrors[question.id] && questionsErrors[question.id].correctAnswer;

    const handleChange = (e) => {
        updateQuestion(question.id, { correctAnswer: e.target.value });
    };

    return (
        <div className="uk-margin">
            <label className="uk-form-label" htmlFor={`question-${question.id}-correct-answer`}>{t('constructor.correctAnswer')}</label>
            <div className="uk-form-controls">
                <select
                    className={`uk-select ${error ? 'uk-form-danger' : ''}`}
                    id={`question-${question.id}-correct-answer`}
                    value={correctAnswer}
                    onChange={handleChange}
                >
                    <option value="undefined" disabled hidden>{t('constructor.correctAnswerPlaceholder')}</option>
                    {answers.map((answer) => (
                        <option key={answer.id} value={answer.id}>{answer.text}</option>
                    ))}
                </select>
                {error && <div className="uk-text-small uk-text-danger">{error}</div>}
            </div>
        </div>

    )
}

const AnswerPrecise = ({ question }) => {
    const { questionsErrors, updateQuestion } = React.useContext(AppContext);
    const { t } = ReactI18next.useTranslation();
    const error = questionsErrors[question.id] && questionsErrors[question.id].correctAnswer;

    const handleChange = (e) => {
        updateQuestion(question.id, { correctAnswer: e.target.value });
    };

    return (
        <div className="uk-margin">
            <label className="uk-form-label" htmlFor={`answer-precise-${question.id}-text`}>{t('constructor.correctAnswer')}</label>
            <div className="uk-form-controls">
                <input
                    type="text"
                    className={`uk-input ${error ? 'uk-form-danger' : ''}`}
                    id={`answer-precise-${question.id}-text`}
                    placeholder={t('constructor.answerTextPlacehoder')}
                    value={question.correctAnswer}
                    onChange={handleChange}
                />
                {error && <div className="uk-text-small uk-text-danger">{error}</div>}
            </div>
        </div>
    )
};

const AnswerText = ({ index, answer, onChange }) => {
    const { t } = ReactI18next.useTranslation();

    const handleChange = (e) => {
        onChange(answer.id, e.target.value);
    };

    return (
        <div className="uk-margin">
            <label className="uk-form-label" htmlFor={`answer-${answer.id}-text`}>{index}. {t('constructor.answerText')}</label>
            <div className="uk-form-controls">
                <input
                    type="text"
                    className="uk-input"
                    id={`answer-${answer.id}-text`}
                    placeholder={t('constructor.answerTextPlacehoder')}
                    value={answer.text}
                    onChange={handleChange}
                />
            </div>
        </div>
    );
};

const QuestionForm = ({ index, question, onSubmit }) => {
    const { setActiveQuestion } = React.useContext(AppContext);
    const [isSubmitting, setSubmitting] = React.useState(false);
    const { t } = ReactI18next.useTranslation();

    const handleSubmit = (e) => {
        e.preventDefault();

        setSubmitting(true);
        onSubmit(question).then((success) => {
            if (success) setActiveQuestion(null);
        }).finally(() => {
            setSubmitting(false);
        });
    };
    const handleClose = () => {
        onSubmit(question);
        setActiveQuestion(null);
    };

    return (
        <div className="uk-card uk-card-default uk-card-body uk-margin">
            <a href="#" className="uk-position-top-right uk-padding" uk-icon="icon: close" onClick={handleClose} />
            <h3 className="uk-card-title">{t('constructor.question')} #{index + 1}</h3>
            <form className="uk-form-stacked" onSubmit={handleSubmit}>
                <QuestionTypeSelector question={question} />
                <QuestionText question={question} />
                <Answer question={question} />
                <div className="uk-text-right">
                    <button className="uk-button uk-button-primary" disabled={isSubmitting}>{t('actions.save')}</button>
                </div>
            </form>
        </div>
    );
};

const NoData = () => {
    const { t } = ReactI18next.useTranslation();

    return (
        <p className="uk-text-italic">{t('constructor.empty')}</p>
    );
}

const QuestionCard = ({ index, question }) => {
    const { questionsErrors, removeQuestion, setActiveQuestion } = React.useContext(AppContext);
    const { t } = ReactI18next.useTranslation();
    const hasErrors = !!questionsErrors[question.id];

    const handleClick = () => {
        setActiveQuestion(question.id);
    };
    const handleRemove = () => {
        removeQuestion(question.id);
    };

    const getQuestionType = () => {
        switch (question.type) {
            case 'options':
                return t('constructor.types.options');
            case 'precise':
                return t('constructor.types.precise');
            default:
                return null;
        }
    };
    const questionType = getQuestionType();
    const getCorrectAnswer = () => {
        switch (question.type) {
            case 'options':
                const answer = question.answers.find(({ id }) => id === question.correctAnswer);
                return answer && answer.text;
            case 'precise':
                return question.correctAnswer;
            default:
                return null;
        }
    };
    const correctAnswer = getCorrectAnswer();
    const answers = question.answers.filter(({ text }) => !!text);

    return (
        <div className="uk-card uk-card-default uk-margin">
            <div className="uk-card-header">
                <h3 className="uk-card-title">{t('constructor.question')} #{index + 1}</h3>
                {hasErrors && (
                    <p className="uk-text-danger uk-flex uk-flex-middle">
                        <span className="uk-margin-small-right" uk-icon="icon: warning" /> {t('constructor.validationError')}
                    </p>
                )}
            </div>
            <div className="uk-card-body">
                <p className="uk-text-bold">{t('constructor.type')}</p>
                {questionType ? <p>{questionType}</p> : <NoData />}
                <p className="uk-text-bold">{t('constructor.text')}</p>
                {question.text ? <p>{question.text}</p> : <NoData />}
                {question.type === 'options' && (
                    <React.Fragment>
                        <p className="uk-text-bold">{t('constructor.answers')}:</p>
                        {answers.length === 0 && <NoData />}
                        <ul>
                            {answers.map((answer) => (
                                <li key={answer.id}>{answer.text}</li>
                            ))}
                        </ul>
                    </React.Fragment>
                )}
                <p className="uk-text-bold">{t('constructor.correctAnswer')}</p>
                {correctAnswer ? <p>{correctAnswer}</p> : <NoData />}
            </div>
            <div className="uk-card-footer uk-flex uk-flex-center">
                <button className="uk-button uk-margin-right" onClick={handleClick}>{t('actions.edit')}</button>
                <button className="uk-button uk-button-danger uk-margin-left" onClick={handleRemove}>{t('actions.delete')}</button>
            </div>
        </div>
    );
};

const Question = ({ index, question, onChange }) => {
    const { activeQuestion } = React.useContext(AppContext);

    return (
        question.id === activeQuestion
            ? <QuestionForm index={index} question={question} onSubmit={onChange} />
            : <QuestionCard index={index} question={question} />
    );
};

const App = ({ testId }) => {
    const [questions, setQuestions] = React.useState([]);
    const [isLoading, setLoading] = React.useState(true);
    const [activeQuestion, setActiveQuestion] = React.useState(null);
    const [questionsErrors, setQuestionsErrors] = React.useState({});
    const { t } = ReactI18next.useTranslation();

    const addQuestion = () => {
        const newQuestion = {
            id: uuidv4(),
            type: null,
            text: '',
            correctAnswer: '',
            answers: [
                { id: uuidv4(), text: '' },
                { id: uuidv4(), text: '' },
            ],
        };
        setQuestions([...questions, newQuestion]);
        setActiveQuestion(newQuestion.id);
    };

    const updateQuestion = (questionId, diff) => {
        setQuestions(
            questions.map((question) => {
                if (question.id !== questionId) return question;

                return { ...question, ...diff };
            })
        );
    };

    const removeQuestion = (questionId) => {
        api.removeQuestion(testId, questionId).then(() => {
            setQuestions(questions.filter(({ id }) => id !== questionId));
        });
    };

    const addErrors = (questionId, errors) => {
        setQuestionsErrors({ ...questionsErrors, [questionId]: errors });
    };

    const saveQuestion = (question) => {
        return api.upsertQuestion(testId, question)
            .then((response) => {
                addErrors(question.id, null);
                return response;
            })
            .catch((error) => {
                if (!error.response) throw(error);

                return error.response.json().then((validationErrors) => {
                    addErrors(question.id, validationErrors);
                    return false;
                });
            });
    };

    React.useEffect(() => {
        api.fetchQuestions(testId).then((response) => {
            if (response.length > 0) {
                setQuestions(response.map((question) => ({ ...question, type: question.type.toLowerCase() })));
            } else {
                addQuestion();
            }

            setTimeout(() => { setLoading(false) }, 1000);
        });
    }, []);

    if (isLoading) {
        return (
            <div className="uk-text-center uk-padding">
                <div uk-spinner="ratio: 3" />
            </div>
        )
    }

    return (
        <AppContext.Provider value={{ activeQuestion, setActiveQuestion, questionsErrors, updateQuestion, removeQuestion }}>
            <div>
                {questions.map((question, index) => (
                    <Question key={question.id} index={index} question={question} onChange={saveQuestion} />
                ))}
            </div>
            <div className="uk-margin uk-flex uk-flex-center">
                <button
                    className="uk-button uk-button-primary"
                    onClick={addQuestion}
                >{t('constructor.addQuestion')}</button>
            </div>
        </AppContext.Provider>
    );
};

const root = document.getElementById('test-constructor');
const testId = root.dataset.testId;
ReactDOM.render(<App testId={testId} />, root);