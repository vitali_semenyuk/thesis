const usersApi = {
    fetch(projectId) {
        return fetch(`/admin/projects/${projectId}/users`)
            .then((response) => response.json());
    },
    assign(projectId, userId) {
        return fetch(`/admin/projects/${projectId}/users/${userId}`, {
            method: 'PUT',
            headers: getCSRFHeaders(),
        });
    },
    unassign(projectId, userId) {
        return fetch(`/admin/projects/${projectId}/users/${userId}`, {
            method: 'DELETE',
            headers: getCSRFHeaders(),
        })
    },
};

const UsersSelectorAppContext = React.createContext({});

const User = ({ user }) => {
    const { projectId, updateUser } = React.useContext(UsersSelectorAppContext);
    const { t } = ReactI18next.useTranslation();

    const handleAddClick = () => {
        usersApi.assign(projectId, user.id).then(() => {
            updateUser(user.id, { projectId });
        });
    };
    const handleRemoveClick = () => {
        usersApi.unassign(projectId, user.id).then(() => {
            updateUser(user.id, { projectId: null });
        });
    };

    return (
        <div className="uk-card uk-card-default uk-margin">
            <div className="uk-card-header">
                <div className="uk-grid-small uk-flex-middle" uk-grid="true">
                    <div className="uk-width-auto">
                        <img className="uk-border-circle" width="40" height="40" src={user.avatarUrl} />
                    </div>
                    <div className="uk-width-expand">
                        <h3 className="uk-card-title uk-margin-remove-bottom">{user.fullName}</h3>
                        <p className="uk-text-meta uk-margin-remove-top">{user.username}</p>
                    </div>
                </div>
            </div>
            <div className="uk-card-body">
                <p>{t('user.location')}: {user.location || '-'}</p>
                <p>{t('user.speciality')}: {user.speciality || '-'}</p>
            </div>
            <div className="uk-card-footer">
                {user.projectId === projectId ? (
                    <a href="#" className="uk-button uk-button-danger" onClick={handleRemoveClick}>{t('actions.remove')}</a>
                ) : (
                    <a href="#" className="uk-button uk-button-primary" onClick={handleAddClick}>{t('actions.add')}</a>
                )}
            </div>
        </div>
    );
};

const UserList = ({ users }) => {
    const { filter } = React.useContext(UsersSelectorAppContext);
    const assigned = users.filter(({ projectId }) => projectId);
    const unassigned = users.filter(({ projectId }) => !projectId);
    const collection = [...assigned, ...unassigned].filter(({ fullName }) => fullName.toLowerCase().includes(filter.toLowerCase()));

    return (
        <div className="uk-height-max-large uk-overflow-auto uk-padding">
            {collection.map((user) => (
                <User key={user.id} user={user} />
            ))}
        </div>
    );
};

const UsersSearchBar = () => {
    const { filter, setFilter } = React.useContext(UsersSelectorAppContext);

    return (
        <SearchBar filter={filter} setFilter={setFilter} />
    );
};

const UsersSelectorApp = ({ projectId }) => {
    const [users, setUsers] = React.useState([]);
    const [isLoading, setLoading] = React.useState(true);
    const [filter, setFilter] = React.useState('');

    const updateUser = (userId, diff) => {
        setUsers(
            users.map((user) => {
                if (user.id !== userId) return user;

                return { ...user, ...diff };
            })
        );
    };

    React.useEffect(() => {
        usersApi.fetch(projectId).then((response) => {
            setUsers(response.map((item) => ({ ...item.user, projectId: item.projectId })));

            setTimeout(() => {
                setLoading(false);
            }, 1000);
        });
    }, []);

    if (isLoading) {
        return (
            <div className="uk-text-center uk-padding">
                <div uk-spinner="ratio: 3" />
            </div>
        );
    }

    return (
        <UsersSelectorAppContext.Provider value={{ projectId, filter, setFilter, updateUser }}>
            <UsersSearchBar />
            <UserList users={users} />
        </UsersSelectorAppContext.Provider>
    );
};

const launchUsersSelector = () => {
    const root = document.getElementById('modal-body');
    const projectId = root.dataset.projectId;
    ReactDOM.render(<UsersSelectorApp projectId={projectId} />, root);
};
