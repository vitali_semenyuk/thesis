function handleSignOut() {
    const form = document.getElementById("signOutForm");
    form.submit();
}

function getCSRFHeaders() {
    const csrfHeader = document.querySelector('meta[name=\'_csrf_header\']').getAttribute('content');
    const csrfValue = document.querySelector('meta[name=\'_csrf\']').getAttribute('content');

    const headers = {};
    headers[csrfHeader] = csrfValue;

    return headers;
}

/*
    Close all open dropdowns before showing a modal
 */
function setupModals() {
    const modals = document.querySelectorAll("[uk-modal]");
    const dropdowns = document.querySelectorAll("[uk-dropdown]");

    if (modals.length === 0 || dropdowns.length === 0) return;

    modals.forEach((modal) => {
        UIkit.util.on(modal, "beforeshow", () => {
            dropdowns.forEach((dropdown) => {
                // false - hide without delay
                UIkit.dropdown(dropdown).hide(false);
            });
        });
    });
}

function setupDeleteModal() {
    const modal = document.getElementById("modal-delete");
    const placeholder = document.getElementById("modal-delete-placeholder");
    const confirmButton = document.getElementById("modal-delete-confirm");
    const buttons = document.querySelectorAll("[data-component-type='delete-button']");

    if (!modal || buttons.length === 0) return;

    buttons.forEach((button) => {
        button.addEventListener("click", (event) => {
            const data = event.target.dataset;
            confirmButton.dataset.entityId = data.entityId;
            confirmButton.dataset.url = data.url;
            if (placeholder) placeholder.innerText = data.name;
            UIkit.modal(modal).show();
        });
    });

    confirmButton.addEventListener("click", (event) => {
        const entityId = event.target.dataset.entityId;
        const url = event.target.dataset.url;

        fetch(`${url}/${entityId}`, {
            method: "DELETE",
            headers: getCSRFHeaders(),
        }).then(() => {
            window.location.reload();
        });
    });
}

function setupGeneralModal() {
    const modal = document.getElementById("modal-general");
    const buttons = document.querySelectorAll("[data-component-type='modal-button-open']");

    if (!modal || buttons.length === 0) return;

    UIkit.util.on(modal, "hidden", () => {
        window.location.reload();
    });

    buttons.forEach((button) => {
        button.addEventListener("click", (event) => {
            const title = modal.querySelector('.uk-modal-title');
            const data = event.target.dataset;
            title.textContent = data.title;
            UIkit.modal(modal).show();

            if (data.app === 'testsSelector') {
                launchTestsSelector();
            } else if (data.app === 'usersSelector') {
                launchUsersSelector();
            }
        });
    })
}

function setupMarkdownEditor() {
    const editor = document.getElementById('markdown-editor');
    if (!editor) return;

    new SimpleMDE({ element: editor });
}

function setupMarkdownRendering() {
    const container = document.getElementById('markdown-content');
    if (!container) return;

    container.innerHTML = marked.parse(container.dataset.markdown, { breaks: true });
}

function setupUploadWizard() {
    const button = document.getElementById('upload_widget');
    const input = document.getElementById('upload_id');
    const preview = document.getElementById('upload_preview');
    if (!button || !input) return;

    const widget = cloudinary.createUploadWidget({
        cloudName: 'itraphotocloud',
        uploadPreset: 't1k10phb',
        sources: ['local', 'url', 'instagram', 'camera'],
        multiple: false,
        resourceType: 'image',
        clientAllowedFormats: ['image'],
        language: 'ru',
    }, (error, result) => {
        if (!error && result && result.event === "success") {
            console.log('Done! Here is the image info: ', result.info);
            input.value = result.info.public_id;
            if (preview) preview.src = result.info.url;
        }
    });

    button.addEventListener("click", () => {
        widget.open();
    }, false);
}

function init() {
    setupModals();
    setupDeleteModal();
    setupGeneralModal();
    setupMarkdownEditor();
    setupMarkdownRendering();
    setupUploadWizard();
}

init();