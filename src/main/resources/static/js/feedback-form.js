const api = {
    getFeedbacks(userId, testId) {
        const params = new URLSearchParams({ userId, testId });
        return fetch('/admin/feedbacks?' + params).then((response) => response.json());
    },
    sendFeedback(text, userId, testId) {
        return fetch('/admin/feedbacks', {
            method: 'POST',
            headers: {
                ...getCSRFHeaders(),
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ userId, testId, text }),
        });
    },
    deleteFeedback(id) {
        return fetch(`/admin/feedbacks/${id}`, {
            method: 'DELETE',
            headers: getCSRFHeaders(),
        });
    },
    getUserFeedbacks(userId) {
        const params = new URLSearchParams({ userId });
        return fetch('/api/feedbacks?' + params).then((response) => response.json());
    }
};

const AppContext = React.createContext({});

const Comment = ({ feedback, onDelete }) => {
    const { t } = ReactI18next.useTranslation();
    const time = moment(feedback.createdAt).locale('ru').fromNow();
    const { readOnly } = React.useContext(AppContext);

    const handleDelete = (e) => {
        e.preventDefault();

        onDelete(feedback.id);
    };

    return (
        <article className="uk-comment uk-comment-primary uk-visible-toggle uk-margin uk-width-large@s" tabIndex="-1">
            <header className="uk-comment-header uk-position-relative">
                <div className="uk-grid-medium uk-flex-middle" uk-grid="true">
                    <div className="uk-width-auto">
                        <img className="uk-comment-avatar" src={feedback.author.avatarUrl} width="80" height="80" alt="" />
                    </div>
                    <div className="uk-width-expand">
                        <h4 className="uk-comment-title uk-margin-remove">
                            <a className="uk-link-reset" href="#">{feedback.author.fullName}</a>
                        </h4>
                        <p className="uk-comment-meta uk-margin-remove-top">
                            <a className="uk-link-reset" href="#">{time}</a>
                        </p>
                    </div>
                </div>
                {!readOnly && (
                    <div className="uk-position-top-right uk-position-small uk-hidden-hover">
                        <a className="uk-link-muted" href="#" onClick={handleDelete}>{t('actions.delete')}</a>
                    </div>
                )}
            </header>
            <div className="uk-comment-body">
                <p>{feedback.text}</p>
            </div>
        </article>
    );
};

const Button = ({ disabled, onClick }) => {
    const { t } = ReactI18next.useTranslation();

    return (
        <button
            className="uk-button"
            disabled={disabled}
            onClick={onClick}
        >{t('feedback.send')}</button>
    );
};

const Textarea = ({ value, disabled, onChange }) => {
    const { t } = ReactI18next.useTranslation();

    return (
        <textarea
            className="uk-textarea"
            value={value}
            placeholder={t('feedback.placeholder')}
            disabled={disabled}
            onChange={onChange}
        />
    );
};

const FeedbackFormApp = ({ testId, userId, currentUserName, currentUserAvatar }) => {
    const [feedbacks, setFeedbacks] = React.useState([]);
    const [feedback, setFeedback] = React.useState('');
    const [isLoading, setLoading] = React.useState(true);
    const [isSubmitting, setSubmitting] = React.useState(false);
    const readOnly = !testId;

    const handleChange = (e) => {
        setFeedback(e.target.value);
    };

    const handleDelete = (feedbackId) => {
        api.deleteFeedback(feedbackId).then(() => {
            setFeedbacks(feedbacks.filter(({ id }) => id !== feedbackId));
        });
    };

    const handleSubmit = () => {
        setSubmitting(true);
        api.sendFeedback(feedback, userId, testId).then(() => {
            setFeedback('');
            setFeedbacks([...feedbacks, {
                id: uuidv4(),
                author: {
                    fullName: currentUserName,
                    avatarUrl: currentUserAvatar,
                },
                text: feedback,
                createdAt: new Date(),
            }]);
        }).finally(() => {
            setSubmitting(false);
        });
    };

    React.useEffect(() => {
        const apiMethod = readOnly ? api.getUserFeedbacks : api.getFeedbacks;
        apiMethod(userId, testId).then((data) => {
            setFeedbacks(data);
            setLoading(false);
        })
    }, []);

    if (isLoading) {
        return (
            <div className="uk-text-center">
                <div uk-spinner="true" />
            </div>
        );
    }

    return (
        <AppContext.Provider value={{ readOnly }}>
            <React.Fragment>
                {!readOnly && <hr className="uk-divider-icon" />}

                <div className="uk-flex uk-flex-column uk-flex-middle">
                    {feedbacks.map((f) => <Comment key={f.id} feedback={f} onDelete={handleDelete} />)}
                </div>

                {!readOnly && (
                    <div className="uk-flex uk-flex-center">
                        <div className="uk-width-large@s">
                            <Textarea value={feedback} disabled={isSubmitting} onChange={handleChange} />
                            <div className="uk-margin">
                                <Button disabled={isSubmitting} onClick={handleSubmit} />
                            </div>
                        </div>
                    </div>
                )}
            </React.Fragment>
        </AppContext.Provider>
    );
};

const launchFeedbackForm = () => {
    const root = document.getElementById('feedback-form');
    if  (!root) return;

    const testId = root.dataset.testId;
    const userId = root.dataset.userId;
    const currentUserName = root.dataset.currentUserName;
    const currentUserAvatar = root.dataset.currentUserAvatar;
    ReactDOM.render(
        <FeedbackFormApp
            testId={testId}
            userId={userId}
            currentUserName={currentUserName}
            currentUserAvatar={currentUserAvatar}
        />,
        root
    );
};

launchFeedbackForm();
